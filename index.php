<?php

$african_animals = array('Bonobo', 'Elephant', 'Lion', 'Mountain Gorilla', 'Rhinoceros', 'Grevy\'s Zebra', 'Chimpanzee', 'Cheetah');
$europe_animals = array('Polar Bear', 'Eurasian Wolf', 'Moose', 'Wild Boar', 'Asp Viper');
$australia_animals = array('Kangaroo', 'Koala', 'Tasmanian Tiger', 'Red Belly', 'Black Snake', 'Red Bellied', 'Blobfish', 'Platypus', 'Cassowary', 'Blue Tongued Lizard');

$animals = array("Africa" => $african_animals, "Europe" => $europe_animals, "Australia" => $australia_animals);

$new_animals_array = [];
foreach ($animals as $location => $animal_array) {
    foreach ($animal_array as $animal) {
        $words = explode(" ", $animal);
        if (count($words) > 1) {
            foreach ($words as $word) {
                array_push($new_animals_array, $word);
            }
        }
    }
}

function print_animals($animals)
{
    for ($i = 0; $i < count($animals); $i++) {
        $j = $i + 1;
        echo "$animals[$i] $animals[$j]<br/>";
        $i++;
    }
}

shuffle($new_animals_array);

print_animals($new_animals_array);