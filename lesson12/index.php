<?php

$options = array(
    PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
);
$pdo = new PDO("mysql:host=127.0.0.1;dbname=global", "khalimov", "", $options);

$task_to_add = isset($_POST['description']) ? $_POST['description'] : null;
$action = isset($_GET['action']) ? $_GET['action'] : null;
$task_id = isset($_GET['id']) ? $_GET['id'] : null;
$edit_value = '';

function delete_task($task_id, $pdo)
{
    $stmt = $pdo->prepare("DELETE FROM tasks WHERE id = ?");
    $stmt->bindParam(1, $task_id);
    $stmt->execute();
}

function accomplish_task($task_id, $pdo)
{
    $stmt = $pdo->prepare("UPDATE tasks SET is_done = TRUE WHERE id = ?");
    $stmt->bindParam(1, $task_id);
    $stmt->execute();
}

function get_edit_description($task_id, $pdo)
{
    $result = $pdo->query("SELECT description FROM tasks WHERE id = $task_id");
    $row = $result->fetch();
    return $row['description'];
}

switch ($action) {
    case 'delete':
        delete_task($task_id, $pdo);
        break;
    case 'done':
        accomplish_task($task_id, $pdo);
        break;
    case 'edit':
        $edit_value = get_edit_description($task_id, $pdo);
        break;
}

if ($task_to_add && $action == 'edit') {
    $stmt = $pdo->prepare("UPDATE tasks SET description = ? WHERE id = ?");
    $stmt->bindParam(1, $task_to_add);
    $stmt->bindParam(2, $task_id);
    $stmt->execute();
    $edit_value = '';
} elseif ($task_to_add) {
    $stmt = $pdo->prepare("INSERT INTO tasks (description, is_done, date_added) VALUES (?, FALSE, NOW())");
    $is_done = false;
    $stmt->bindParam(1, $task_to_add);
    $stmt->execute();
}

if (isset($_POST['sort_by'])) {
    $order = $_POST['sort_by'];
    $tasks = $pdo->query("SELECT * FROM tasks ORDER BY $order ASC");
    var_dump($_POST['sort_by']);
} else {
    $tasks = $pdo->query("SELECT * FROM tasks");
}

?>

<html>
<head>
    <style>
        table {
            border-spacing: 0;
            border-collapse: collapse;
        }

        table td, table th {
            border: 1px solid #ccc;
            padding: 5px;
        }

        table th {
            background: #eee;
        }
    </style>
</head>
<body>
<h1>Список дел на сегодня</h1>
<div style="float: left">
    <form method="POST">
        <input type="text" name="description" placeholder="Описание задачи" value="<?= $edit_value ?>">
        <input type="submit" name="save" value="Добавить">
    </form>
</div>
<div style="float: left; margin-left: 20px;">
    <form method="POST" action="index.php">
        <label for="sort">Сортировать по:</label>
        <select id="sort" name="sort_by">
            <option value="date_added">Дате добавления</option>
            <option value="is_done">Статусу</option>
            <option value="description">Описанию</option>
        </select>
        <input type="submit" name="sort" value="Отсортировать">
    </form>
</div>
<div style="clear: both"></div>

<table>
    <tbody>
    <tr>
        <th>Описание задачи</th>
        <th>Дата добавления</th>
        <th>Статус</th>
        <th></th>
    </tr>

    <?php while ($row = $tasks->fetch()): ?>
        <tr>
            <td><?= $row['description'] ?></td>
            <td><?= $row['date_added'] ?></td>
            <td>
                <?php if ($row['is_done']): ?>
                    <span style="color: green;">Выполнено</span>
                <?php else: ?>
                    <span style="color: orange;">В процессе</span>
                <?php endif; ?>
            </td>
            <td>
                <a href="?id=<?= $row['id'] ?>&amp;action=edit">Изменить</a>
                <a href="?id=<?= $row['id'] ?>&amp;action=done">Выполнить</a>
                <a href="?id=<?= $row['id'] ?>&amp;action=delete">Удалить</a>
            </td>
        </tr>
    <?php endwhile; ?>

    </tbody>
</table>
<div id="feedly-mini" title="feedly Mini tookit"></div>
</body>
</html>
