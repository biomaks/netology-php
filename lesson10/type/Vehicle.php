<?php
namespace type;

interface Vehicle
{
    public function drive();
}