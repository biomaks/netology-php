<?php

error_reporting(E_ALL);

spl_autoload_register(function ($className) {
    $filePath = $_SERVER['DOCUMENT_ROOT'] . '/lesson10/classes/' . $className . '.php';
    if (file_exists($filePath)) {
        include $filePath;
    }
});

spl_autoload_register(function ($className) {
    $filePath = $_SERVER['DOCUMENT_ROOT'] . '/lesson10/core/' . $className . '.php';
    if (file_exists($filePath)) {
        include $filePath;
    }
});

spl_autoload_register(function ($classNameWithNamespace) {
    $filePath = $_SERVER['DOCUMENT_ROOT'] . '/lesson10/' . str_replace('\\', DIRECTORY_SEPARATOR, $classNameWithNamespace) . '.php';
    if (file_exists($filePath)) {
        include $filePath;
    } else {
        die("Class not Found");
    }
});
