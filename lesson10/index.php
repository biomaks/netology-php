<?php

require_once 'main.conf.php';
use depot\Bus as Bus;
use depot\Driver;
use street\Passenger;

$pass1 = new Passenger("Tom");
$pass2 = new Passenger("Dan");
$driver = new Driver("Bob");
$bus = new Bus($driver);

$bus->addPassenger($pass1);
$bus->addPassenger($pass2);

echo $bus->drive();


