<?php

namespace depot;

use type\Vehicle;


class Bus implements Vehicle
{
    private $driver;
    private $passengers = array();

    public function __construct($driver)
    {
        if (is_a($driver, Driver::class)) {
            $this->setDriver($driver);
        } else {
            throw new \Exception("Instance of class is not Driver");
        }
    }

    public function getDriver()
    {
        return $this->driver;
    }

    public function setDriver($driver)
    {
        $this->driver = $driver;
    }

    public function drive()
    {
        return "My driver is " . $this->driver->name . " and I have " . count($this->getPassengers()) . " passengers";
    }

    public function addPassenger($passenger)
    {
        $this->passengers[$passenger->name] = $passenger;
    }

    public function deletePassenger($passenger)
    {
        unset($passenger->name);
    }

    public function getPassengers()
    {
        return $this->passengers;
    }
}

