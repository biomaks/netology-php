<?php
$name = "Erik";
$city = "Winnipeg";
$age = 34;
$email = "erikstb@yandex.ru";
$about = "LAMP developer";
 ?>
 <!DOCTYPE html>
 <html>
   <head>
     <meta charset="utf-8">
     <title>Homework1</title>
     <style>
          body {
              font-family: sans-serif;
          }
          dl {
              display: table-row;
          }
          dt, dd {
              display: table-cell;
              padding: 5px 10px;
          }
     </style>
   </head>
   <body>
     <h2>User <?= $name ?></h2>
     <dl>
       <dt>Name</dt>
       <dd><?= $name ?></dd>
    </dl>
    <dl>
       <dt>City</dt>
       <dd><?= $city ?></dd>
    </dl>
    <dl>
       <dt>Age</dt>
       <dd><?= $age ?></dd>
    </dl>
    <dl>
       <dt>Email</dt>
       <dd><?= $email ?></dd>
    </dl>
    <dl>
       <dt>About</dt>
       <dd><?= $about ?></dd>
     </dl>

   </body>
 </html>