<?php

$test = isset($_GET['test']) ? $_GET['test'] : "";
$name = isset($_GET['name']) ? $_GET['name'] : "";
$show_result = false;

if (!is_test_exist($test) and isset($_GET['test'])) {
    header($_SERVER["SERVER_PROTOCOL"]." 404 Not Found", true, 404);
    exit();
}

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $answer = $_POST['answer'];
    $post_test = $_POST['test'];
    $json = handle_test($post_test);
    $test = $_POST['test'];
    $result = $json['answer'] == $answer;
    $name = $_POST['name'];
    $show_result = true;
}
function is_test_exist($test){
    return file_exists(__DIR__."/files/".$test.".json");
}

function handle_test($test)
{
    $file_name = "files/" . $test . ".json";
    $file = file_get_contents($file_name);
    $json = json_decode($file, true);
    return $json;
}

$test_p = handle_test($test);



?>

<html>
<head></head>
<body>

Question: <?= $test_p["question"] ?>
<form action="test.php" method="post">
    <input type="hidden" name="test" value="<?= $test ?>"/>
    <label for="answer">Answer</label>
    <input id="answer" type="text" name="answer"/>
    <br/>
    <label for="name">Name</label>
    <input id="name" type="text" name="name"/>
    <input type="submit" value="submit"/>
</form>
<br/>
<?php
if ($show_result) {
    if ($result) {
        echo "Correct answer!<br/>";
        echo "<img src=\"image.php?name=$name\" alt=\"Certificate\">";
    } else {
        echo "Wrong answer!";
    }
}
?>
</body>
</html>
