<html>
<head></head>
<body>
<h2> Tests list </h2>
<?php

$files = scandir("files");

echo "<ul>";
for ($i = 0; $i < count($files); $i++) {
    $file = $files[$i];
    if (!in_array($file, array(".", ".."))) {
        $param = substr($file, 0, -5);
        $testNumber = $i - 1;
        echo "<li><a href=\"test.php?test=$param\">Test #$testNumber</a></li>";
    }
}
echo "</ul>";
?>
</body>
</html>