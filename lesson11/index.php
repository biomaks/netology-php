<?php

$isbn = isset($_GET['isbn']) ? $_GET['isbn'] : '';
$name = isset($_GET['name']) ? $_GET['name'] : '';
$author = isset($_GET['author']) ? $_GET['author'] : '';

$options = array(
    PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
);
$pdo = new PDO("mysql:host=localhost;dbname=global", "khalimov", "neto0641", $options);
$stmt = $pdo->query("SELECT * FROM books as b WHERE b.isbn LIKE '%$isbn%' AND b.name LIKE '%$name%' AND b.author LIKE '%$author%'");
?>
<!doctype html>
<html>
<head>
    <style>
        table {
            border-spacing: 0;
            border-collapse: collapse;
        }

        table td, table th {
            border: 1px solid #ccc;
            padding: 5px;
        }

        table th {
            background: #eee;
        }
    </style>
    <title>Document</title>
</head>
<body>
<h1>Библиотека успешного человека</h1>
<form method="GET">
    <input type="text" name="isbn" placeholder="ISBN" value="">
    <input type="text" name="name" placeholder="Название книги" value="">
    <input type="text" name="author" placeholder="Автор книги" value="">
    <input type="submit" value="Поиск">
</form>

<table>
    <tbody>
    <tr>
        <th>Name</th>
        <th>Author</th>
        <th>Year</th>
        <th>Genre</th>
        <th>ISBN</th>
    </tr>
    <?php while ($row = $stmt->fetch()): ?>
        <tr>
            <td><?= $row["name"] ?></td>
            <td><?= $row["author"] ?></td>
            <td><?= $row["year"] ?></td>
            <td><?= $row["genre"] ?></td>
            <td><?= $row["isbn"] ?></td>
        </tr>
    <?php endwhile; ?>

    </tbody>
</table>

</body>
</html>
