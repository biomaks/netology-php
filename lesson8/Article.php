<?php

class Article
{

    private $date;
    private $title;
    private $text;

    public function __construct($date, $title, $text)
    {
        $this->date = $date;
        $this->title = $title;
        $this->text = $text;
    }

    public function getDate()
    {
        return $this->date;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function getText()
    {
        return $this->text;
    }


}