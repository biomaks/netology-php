<?php

class Human extends Animal
{
    private $gender;
    private $age;

    public function __construct($gender, $age, $name)
    {
        parent::__construct($name);
        $this->gender = $gender;
        $this->age = $age;
    }

    public function getGender()
    {
        return $this->gender;
    }

    public function getAge()
    {
        return $this->age;
    }
}