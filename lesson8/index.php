<?php
require "Article.php";

$articles = [new Article("10/10/2016", "Article 1", "article text"), new Article("10/10/2016", "Article 2", "article text")];


?>

<html>
<head></head>
<body>
<h1>Breaking News</h1>
<br>

<?php foreach ($articles as $article): ?>
<article>
    <h2><?= $article->getTitle() ?></h2>
    <h3><?= $article->getDate() ?></h3>
    <div class="content"><?= $article->getText() ?></div>
</article>
<?php endforeach;?>
</body>

</html>
