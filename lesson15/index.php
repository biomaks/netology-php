<?php
error_reporting(E_ERROR | E_PARSE);
require './vendor/autoload.php';

$address = isset($_POST['address']) ? $_POST['address'] : '';
$api = new \Yandex\Geo\Api();

if ($address) {
    $api->setQuery($address);
    $api
        ->setLimit(1)
        ->setLang(\Yandex\Geo\Api::LANG_US)
        ->load();
    $response = $api->getResponse();
    $lat = $response->getFirst()->getLatitude();
    $long = $response->getFirst()->getLongitude();

} else {
    $lat = 55.76;
    $long = 37.64;
}

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <script src="https://api-maps.yandex.ru/2.1/?lang=en_CA" type="text/javascript"></script>
    <title>Ya Map</title>
    <style>
        div {
            margin: 15px;
        }
    </style>
</head>
<body>
<div class="search">
    <form method="post">
        <label for="address">Find address:</label>
        <input id="address" type="text" name="address" value="<?= $address ?>">
        <input type="submit" value="find">
    </form>

</div>
<div id="map" style="width: 600px; height: 400px"></div>
<script type="text/javascript">
    ymaps.ready(init);
    var myMap;

    function init() {
        myMap = new ymaps.Map("map", {
            center: [<?= $lat?>, <?= $long?>],
            zoom: 16
        });

        myPlacemark = new ymaps.Placemark([<?= $lat?>, <?= $long?>], {
            hintContent: 'Your search',
            balloonContent: '<?= $address?>'
        });

        myMap.geoObjects.add(myPlacemark);
    }
</script>
</body>
</html>
