<?php

$test = isset($_GET['test']) ? $_GET['test'] : "";
$show_result = false;

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $answer = $_POST['answer'];
    $post_test = $_POST['test'];
    $json = handle_test($post_test);
    $test = $_POST['test'];
    $result = $json['answer'] == $answer;
    $show_result = true;
}

function handle_test($test)
{
    $file_name = "files/" . $test . '.json';
    $file = file_get_contents($file_name);
    $json = json_decode($file, true);
    return $json;
}

$test_p = handle_test($test);

?>

<html>
<head></head>
<body>

Question: <?= $test_p["question"] ?>
<form action="test.php" method="post">
    <input type="hidden" name="test" value="<?= $test ?>"/>
    <label for="answer">Answer</label>
    <input id="answer" type="text" name="answer"/>
    <input type="submit" value="submit"/>
</form>
<br/>
<?
if ($show_result) {
    if ($result) {
        echo "Correct answer!";
    } else {
        echo "Wrong answer!";
    }
}
?>
</body>
</html>
