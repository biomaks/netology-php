<?php

class Car extends Vehicle
{
    public $model;
    public $owner;

    public function __construct($speed, $distance, $weight, $model)
    {
        parent::__construct($speed, $distance, $weight);
        $this->model = $model;
    }

}