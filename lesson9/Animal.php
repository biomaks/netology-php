<?php


interface AnimalInterface
{
    public function say();
}

class Animal implements AnimalInterface
{
    public $name;

    public function __construct($name)
    {
        return $this->name = $name;
    }

    public function say()
    {
        echo "I'm animal";
    }
}