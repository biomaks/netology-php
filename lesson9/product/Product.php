<?php

interface ProductInterface
{
    public function getPrice();

    public function getDeliveryCost();
}


abstract class Product implements ProductInterface
{
    public $price;
    public $weight;
    private $deliveryCost;
    private $discount;

    public function __construct($price, $weight)
    {
        $this->price = $price;
        $this->weight = $weight;
        if ($this->discount > 0) {
            $this->setDeliveryCost(300);
        } else {
            $this->setDeliveryCost(250);
        }
    }

    abstract public function getPrice();

    public function getDeliveryCost()
    {
        return $this->deliveryCost;
    }

    public function setDeliveryCost($deliveryCost)
    {
        $this->deliveryCost = $deliveryCost;
    }

    public function getDiscount()
    {
        return $this->discount;
    }

    public function setDiscount($discount)
    {
        $this->discount = $discount;
    }

}