<?php

class Bread extends Product
{
    public function __construct($price, $weight)
    {
        parent::__construct($price, $weight);
        if ($weight > 10) {
            $this->setDiscount(10);
        } else {
            $this->setDiscount(0);
        }
    }

    public function getPrice()
    {
        return $this->price - $this->price / 100 * $this->getDiscount();
    }
}