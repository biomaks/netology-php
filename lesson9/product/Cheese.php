<?php

class Cheese extends Product
{

    public function __construct($price, $weight)
    {
        parent::__construct($price, $weight);
    }

    public function getPrice()
    {
        return $this->price - $this->price / 100 * $this->getDiscount();
    }
}