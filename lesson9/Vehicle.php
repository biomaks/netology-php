<?php

interface VehicleInterface
{
    public function drive();
}

class Vehicle implements VehicleInterface
{
    public $speed;
    public $distance;
    public $weight;

    public function __construct($speed, $distance, $weight)
    {
        $this->speed = $speed;
        $this->distance = $distance;
        $this->weight = $weight;

    }

    public function drive()
    {
        return "I drive for " . $this->distance . ", my speed is " . $this->speed . " km/h";
    }

}