<?php

class Truck extends Vehicle
{
    public $heavyWeight;

    public function __construct($speed, $distance, $weight)
    {
        parent::__construct($speed, $distance, $weight);
        if ($weight > 5000) {
            $this->heavyWeight = true;
        } else {
            $this->heavyWeight = false;
        }
    }
}
