<?php
$cont = file_get_contents("text.txt");

function check_files($dirname)
{
    $data = array();
    $j = 1;
    while (file_exists($dirname . "/" . $j . ".jpg")) {
        $filename = $dirname . "/" . $j . ".jpg";
        $size = filesize($filename);
        $ch_date = filemtime($filename);
        $file_data = array($filename, $size, $ch_date);
        array_push($data, $file_data);
        $j++;
    }
    $fp = fopen("images.csv", "w");

    foreach ($data as $item) {
        fputcsv($fp, $item);
    }

    fclose($fp);
}

check_files("img");
