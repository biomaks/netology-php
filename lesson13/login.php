<?php
require_once 'main.conf.php';
session_start();
$login = isset($_POST['login']) ? $_POST['login'] : '';
$password = isset($_POST['password']) ? $_POST['password'] : '';
$action = isset($_POST['action']) ? $_POST['action'] : null;
$error = false;
switch ($action) {
    case 'register' :
        register($login, $password);
        break;
    case 'login' :
        login($login, $password);
        break;
}

function login($login, $password)
{
    $userRepo = new UserRepository();
    $user = $userRepo->getUserByLogin($login);
    if ($user && $password == $user->getPassword()) {
        $_SESSION['isAuthorised'] = true;
        $_SESSION['username'] = $login;
        $_SESSION['currentUserId'] = $user->getId();
        header("Location: index.php");
        exit();
    } else {
        global $error;
        $error = true;
    }
}

function register($login, $password)
{
    $userRepo = new UserRepository();
    $user = new User(null, $login, $password);
    $userRepo->save($user);
    $user = $userRepo->getUserByLogin($login);
    $_SESSION['isAuthorised'] = true;
    $_SESSION['username'] = $login;
    $_SESSION['currentUserId'] = $user->getId();
    header("Location: index.php");
    exit();
}

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Login</title>
</head>
<body>
<?php if ($error): ?>
    <h3 style="color: red">Wrong username/password</h3>
<?php endif; ?>
<form method="post">
    <table>
        <tr>
            <td>
                <label for="login">Login: </label>
            </td>
            <td>
                <input id="login" name="login" type="text" value=""/>
            </td>
        </tr>
        <tr>
            <td>

                <label for="password">Password: </label>
            </td>
            <td>
                <input id="password" name="password" type="password" value=""/>
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
                <input name="action" type="submit" value="login">
                <input name="action" type="submit" value="register">
            </td>
        </tr>
    </table>
</form>
</body>
</html>


