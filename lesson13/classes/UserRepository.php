<?php

class UserRepository extends BaseRepo implements Repository
{
    public function __construct()
    {
        parent::__construct();
    }

    public function save($user)
    {
        $login = $user->getLogin();
        $password = $user->getPassword();
        $stmt = $this->pdo->prepare("INSERT INTO user (login, password) VALUES (?, ?)");
        $stmt->bindParam(1, $login);
        $stmt->bindParam(2, $password);
        $stmt->execute();
    }

    public function getAll()
    {
        $result = $this->pdo->query("SELECT * FROM user ORDER BY login ASC");
        $users = array();
        while ($row = $result->fetch()) {
            $user = new User($row['id'], $row['login'], $row['password']);
            array_push($users, $user);
        }
        return $users;
    }


    public function getById($id)
    {
        $result = $this->pdo->query("SELECT * FROM user WHERE id = $id");
        $record = $result->fetch();
        $user = new User($record['id'], $record['login'], $record['password']);
        return $user;
    }

    public function getUserByLogin($login)
    {
        $stmt = $this->pdo->prepare("SELECT * FROM user WHERE login = ?");
        $stmt->bindParam(1, $login);
        $stmt->execute();
        if ($stmt->rowCount() < 1) {
            return null;
        } else {
            $record = $stmt->fetch();
            $user = new User($record['id'], $record['login'], $record['password']);
            return $user;
        }
    }
}