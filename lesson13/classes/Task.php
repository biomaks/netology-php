<?php

class Task
{
    private $id;
    private $userId;
    private $userName;
    private $assignedUserId;
    private $assignedUserName;
    private $description;
    private $isDone;
    private $dateAdded;
    private $user;
    private $assignedUser;


    public function __construct($id, $userId, $assignedUserId, $description, $isDone, $dateAdded)
    {
        $this->id = $id;
        $this->userId = $userId;
        $this->assignedUserId = $assignedUserId;
        $this->description = $description;
        $this->isDone = $isDone;
        $this->dateAdded = $dateAdded;

    }

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getUserId()
    {
        return $this->userId;
    }

    public function setUserId($userId)
    {
        $this->userId = $userId;
    }

    public function getAssignedUserId()
    {
        return $this->assignedUserId;
    }

    public function setAssignedUserId($assignedUserId)
    {
        $this->assignedUserId = $assignedUserId;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setDescription($description)
    {
        $this->description = $description;
    }

    public function getIsDone()
    {
        return $this->isDone;
    }

    public function setIsDone($isDone)
    {
        $this->isDone = $isDone;
    }

    public function getDateAdded()
    {
        return $this->dateAdded;
    }

    public function setDateAdded($dateAdded)
    {
        $this->dateAdded = $dateAdded;
    }

    public function getUser()
    {
        return $this->user;
    }

    public function setUser($user)
    {
        $this->user = $user;
    }

    public function getAssignedUser()
    {
        return $this->assignedUser;
    }

    public function setAssignedUser($assignedUser)
    {
        $this->assignedUser = $assignedUser;
    }

    public function getUserName()
    {
        return $this->userName;
    }

    public function setUserName($userName)
    {
        $this->userName = $userName;
    }

    public function getAssignedUserName()
    {
        return $this->assignedUserName;
    }

    public function setAssignedUserName($assignedUserName)
    {
        $this->assignedUserName = $assignedUserName;
    }


}