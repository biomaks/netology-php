<?php

class TaskRepository extends BaseRepo implements Repository
{

    public function __construct()
    {
        parent::__construct();
    }

    public function save($task)
    {
        $userId = $task->getUserId();
        $assignedUserId = $task->getAssignedUserId();
        $description = $task->getDescription();
        $isDone = $task->getIsDone();
        $stmt = $this->pdo->prepare("INSERT INTO task (user_id, assigned_user_id, description, is_done, date_added) VALUES (?,?,?,?, NOW())");
        $stmt->bindParam(1, $userId);
        $stmt->bindParam(2, $assignedUserId);
        $stmt->bindParam(3, $description);
        $stmt->bindParam(4, $isDone);
        $stmt->execute();

    }

    public function getById($id)
    {
        $result = $this->pdo->query("SELECT * FROM task WHERE id = $id");
        $record = $result->fetch();
        $task = new Task($record['id'], $record['user_id'], $record['assigned_user_id'], $record['description'], $record['is_done'], $record['date_added']);
        return $task;
    }

    public function getAllOwnedByUser($order, $userId)
    {
        $result = $this->pdo->query("SELECT * FROM task WHERE user_id = $userId ORDER BY $order ASC");
        $tasks = array();
        $userRepository = new UserRepository();
        if (count($result) > 0) {
            while ($row = $result->fetch()) {
                $task = new Task($row['id'], $row['user_id'], $row['assigned_user_id'], $row['description'], $row['is_done'], $row['date_added']);
                $user = $userRepository->getById($row['user_id']);
                $assignedUser = $userRepository->getById($row['assigned_user_id']);
                $task->setUser($user);
                $task->setAssignedUser($assignedUser);
                array_push($tasks, $task);
            }
        }
        return $tasks;
    }

    public function getAllAssignedToMe($order, $assignedUserId)
    {
        $result = $this->pdo->query("SELECT t.id AS task_id, t.assigned_user_id, t.description, t.is_done, t.date_added, u.id AS owner_id, u.login AS owner_name FROM task AS t INNER JOIN user AS u ON t.user_id = u.id WHERE t.assigned_user_id = $assignedUserId AND t.user_id NOT IN ($assignedUserId) ORDER BY $order");
        $tasks = array();
        $userRepository = new UserRepository();
        if (count($result) > 0) {
            while ($row = $result->fetch()) {
                $task = new Task($row['task_id'], $row['owner_id'], $row['assigned_user_id'], $row['description'], $row['is_done'], $row['date_added']);
                $assignedUser = $userRepository->getById($row['assigned_user_id']);
                $task->setUserName($row['owner_name']);
                $task->setAssignedUser($assignedUser);
                $task->setAssignedUserName($assignedUser->getLogin());
                array_push($tasks, $task);
            }
        }
        return $tasks;
    }

    public function updateTask($task)
    {
        $id = $task->getId();
        $userId = $task->getUserId();
        $assignedUserId = $task->getAssignedUserId();
        $description = $task->getDescription();
        $isDone = $task->getIsDone();
        $dateAdded = $task->getDateAdded();
        $stmt = $this->pdo->prepare("UPDATE task SET user_id=?, assigned_user_id=?, description=?, is_done=?, date_added=? WHERE id = ?");
        $stmt->bindParam(1, $userId);
        $stmt->bindParam(2, $assignedUserId);
        $stmt->bindParam(3, $description);
        $stmt->bindParam(4, $isDone);
        $stmt->bindParam(5, $dateAdded);
        $stmt->bindParam(6, $id);
        $stmt->execute();
    }

    public function deleteTask($task)
    {
        $id = $task->getId();
        $stmt = $this->pdo->prepare("DELETE FROM task WHERE id = ?");
        $stmt->bindParam(1, $id);
        $stmt->execute();
    }
}