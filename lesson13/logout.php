<?php

session_start();
unset($_SESSION["isAuthorised"]);
unset($_SESSION["username"]);
unset($_SESSION["currentUserId"]);

header("Location: index.php");
exit();