<?php

require_once 'main.conf.php';

session_start();
$isAuthorised = isset($_SESSION['isAuthorised']) ? $_SESSION['isAuthorised'] : null;

if ($isAuthorised == null) {
    header("Location: login.php");
    exit();
}
$userRepository = new UserRepository();

$currentUserId = isset($_SESSION['currentUserId']) ? $_SESSION['currentUserId'] : null;
$userIdToReAssign = isset($_POST['assigned_user_id']) ? $_POST['assigned_user_id'] : null;
$action = isset($_GET['action']) ? $_GET['action'] : null;
$taskId = isset($_GET['id']) ? $_GET['id'] : null;
$taskToAdd = isset($_POST['description']) ? $_POST['description'] : null;
$task = null;
$editValue = '';
$currentUser = $userRepository->getById($currentUserId);
$allUsers = $userRepository->getAll();
$taskRepository = new TaskRepository();

if ($userIdToReAssign) {
    $taskId = $_POST['task_id'];
    $userToReAssign = $userRepository->getById($userIdToReAssign);
}

if ($taskId) {
    $task = $taskRepository->getById($taskId);
}


switch ($action) {
    case 'delete':
        $taskRepository->deleteTask($task);
        break;
    case 'done':
        $task->setIsDone(true);
        $taskRepository->updateTask($task);
        break;
    case 'edit':
        $editValue = $task->getDescription();
        break;
}

if ($taskToAdd && $action == 'edit') {
    $task->setDescription($taskToAdd);
    $taskRepository->updateTask($task);
    header("Location: index.php");
    exit();
} elseif ($taskToAdd) {
    $task = new Task(null, $currentUser->getId(), $currentUser->getId(), $taskToAdd, false, null);
    $taskRepository->save($task);
} elseif ($userIdToReAssign) {
    $task->setAssignedUserId($userIdToReAssign);
    $taskRepository->updateTask($task);
}


if (isset($_POST['sort_by'])) {
    $order = $_POST['sort_by'];
    $tasks = $taskRepository->getAllOwnedByUser($order, $currentUserId);
    $myTasks = $taskRepository->getAllAssignedToMe($order, $currentUserId);
} else {
    $order = 'date_added';
    $tasks = $taskRepository->getAllOwnedByUser($order, $currentUserId);
    $myTasks = $taskRepository->getAllAssignedToMe($order, $currentUserId);
}

?>

<html>
<head>
    <style>
        table {
            border-spacing: 0;
            border-collapse: collapse;
        }

        table td, table th {
            border: 1px solid #ccc;
            padding: 5px;
        }

        table th {
            background: #eee;
        }
    </style>
</head>
<body>
<h1>Список дел на сегодня</h1>
<div style="float: left">
    <form method="POST">
        <input type="text" name="description" placeholder="Описание задачи" value="<?= $editValue ?>">
        <input type="submit" name="save" value="Добавить">
    </form>
</div>
<div style="float: left; margin-left: 20px;">
    <form method="POST" action="index.php">
        <label for="sort">Сортировать по:</label>
        <select id="sort" name="sort_by">
            <option value="date_added">Дате добавления</option>
            <option value="is_done">Статусу</option>
            <option value="description">Описанию</option>
        </select>
        <input type="submit" name="sort" value="Отсортировать">
    </form>
</div>
<div style="clear: both"></div>

<table>
    <tbody>
    <tr>
        <th>Описание задачи</th>
        <th>Дата добавления</th>
        <th>Статус</th>
        <th></th>
        <th>Ответственный</th>
        <th>Автор</th>
        <th>Закрепить задачу за пользователем</th>
    </tr>

    <?php foreach ($tasks as $row): ?>
        <tr>
            <td><?= $row->getDescription() ?></td>
            <td><?= $row->getDateAdded() ?></td>
            <td>
                <?php if ($row->getIsDone()): ?>
                    <span style="color: green;">Выполнено</span>
                <?php else: ?>
                    <span style="color: orange;">В процессе</span>
                <?php endif; ?>
            </td>
            <td>
                <a href="?id=<?= $row->getId() ?>&amp;action=edit">Изменить</a>
                <a href="?id=<?= $row->getId() ?>&amp;action=done">Выполнить</a>
                <a href="?id=<?= $row->getId() ?>&amp;action=delete">Удалить</a>
            </td>
            <td><?= $row->getAssignedUser()->getLogin() ?></td>
            <td><?= $row->getUser()->getLogin() ?></td>
            <td>
                <form method="POST">
                    <select name="assigned_user_id">
                        <?php foreach ($allUsers as $allUser): ?>
                            <option value="<?= $allUser->getId() ?>"><?= $allUser->getLogin() ?></option>
                        <?php endforeach; ?>
                    </select>
                    <input type="hidden" name="task_id" value="<?= $row->getId() ?>">
                    <input type="submit" name="assign" value="Переложить ответственность"></form>


            </td>
        </tr>
    <?php endforeach; ?>

    </tbody>
</table>

<p><strong>Также, посмотрите, что от Вас требуют другие люди:</strong></p>

<table>
    <tbody>
    <tr>
        <th>Описание задачи</th>
        <th>Дата добавления</th>
        <th>Статус</th>
        <th></th>
        <th>Ответственный</th>
        <th>Автор</th>
    </tr>

    <?php foreach ($myTasks as $row): ?>
        <tr>
            <td><?= $row->getDescription() ?></td>
            <td><?= $row->getDateAdded() ?></td>
            <td>
                <?php if ($row->getIsDone()): ?>
                    <span style="color: green;">Выполнено</span>
                <?php else: ?>
                    <span style="color: orange;">В процессе</span>
                <?php endif; ?>
            </td>
            <td>
                <a href="?id=<?= $row->getId() ?>&amp;action=edit">Изменить</a>
                <a href="?id=<?= $row->getId() ?>&amp;action=done">Выполнить</a>
                <a href="?id=<?= $row->getId() ?>&amp;action=delete">Удалить</a>
            </td>
            <td><?= $row->getAssignedUserName() ?></td>
            <td><?= $row->getUserName() ?></td>
        </tr>
    <?php endforeach; ?>

    </tbody>
</table>

<p><a href="/lesson13/logout.php">Выход</a></p>
</body>
</html>




