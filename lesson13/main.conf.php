<?php

error_reporting(E_ALL);

spl_autoload_register(function ($className) {
    $filePath = $_SERVER['DOCUMENT_ROOT'] . '/lesson13/classes/' . $className . '.php';
    if (file_exists($filePath)) {
        include $filePath;
    }
});


