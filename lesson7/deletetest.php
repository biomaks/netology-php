<?php
session_start();
if ($_SESSION["role"] != "admin") {
    header($_SERVER["SERVER_PROTOCOL"] . " 403 Forbidden", true, 403);
    echo "<h1>403 Forbidden</h1>";
    exit();
}
$filename = __DIR__ . "/files/" . $_GET["file_id"] . ".json";
unlink($filename);
header("Location: list.php");