<?php
session_start();
$role = isset($_SESSION["role"]) ? $_SESSION["role"] : null;
if ($role != "admin") {
    header($_SERVER["SERVER_PROTOCOL"] . " 403 Forbidden", true, 403);
    echo "<h1>403 Forbidden</h1>";
    exit();
}

$dir_path = __DIR__ . '/files/';
$file_name = uniqid() . '.json';
if (!empty($_FILES['user_file'])) {
    $user_file = $_FILES['user_file']['tmp_name'];
    if (move_uploaded_file($user_file, $dir_path . $file_name)) {
        header("Location: list.php");
        exit();
    } else {
        echo "Error " . $user_file['error'];
    }
}
?>

<html>
<head>
</head>
<body>
<form action="admin.php" enctype="multipart/form-data" method="post">
    <input type="file" name="user_file">
    <input type="submit" value="submit">
</form>
</body>
</html>
