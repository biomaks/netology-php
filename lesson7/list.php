<html>
<head></head>
<body>
<h2> Tests list </h2>
<?php
session_start();
$role = isset($_SESSION["role"])? $_SESSION["role"] : "";
$is_admin = $role == "admin";

$files = scandir("files");

if ($is_admin) {
    echo "<a href='admin.php'>add test</a>";
}

echo "<ul>";
for ($i = 0; $i < count($files); $i++) {
    $file = $files[$i];
    if (!in_array($file, array(".", ".."))) {
        $param = substr($file, 0, -5);
        $testNumber = $i - 1;
        if ($is_admin) {
            echo "<li><a href=\"test.php?test=$param\">Test #$testNumber</a> | <a href=\"deletetest.php?file_id=$param\">remove file</a> </li>";
        } else {
            echo "<li><a href=\"test.php?test=$param\">Test #$testNumber</a></li>";
        }
    }
}
echo "</ul>";
?>
</body>
</html>