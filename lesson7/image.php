<?php
$name = $_GET['name'];

function create_image($username)
{
    $image = @imagecreatetruecolor(600, 600) or die("ERRER");
    $backgroundcolor = imagecolorallocate($image, 51, 51, 255);
    $textcolor = imagecolorallocate($image, 255, 254, 240);
    $font_path = realpath(__DIR__ . "/font.ttf");
    imagefill($image, 0, 0, $backgroundcolor);
    imagettftext($image, 40, 0, 150, 100, $textcolor, $font_path, "Certificate");
    $text = "Congrats $username you've done it!";
    imagettftext($image, 20, 0, 80, 240, $textcolor, $font_path, $text);
    imagepng($image);
}

header('Content-type: image/jpeg');

create_image($name);