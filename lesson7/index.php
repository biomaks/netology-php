<?php

session_start();

$name = isset($_POST["name"]) ? $_POST["name"] : "";
$password = isset($_POST["password"])? $_POST["password"] : null;

if (isset($_POST["name"]) && isset($_POST["password"])) {
    if (user_is_valid($_POST["name"], $_POST["password"])) {
        $_SESSION["name"] = $name;
        $_SESSION["role"] = "admin";
        header("Location: list.php");
    }
}
if (strlen($name) > 0 and strlen($password) == 0) {
    $_SESSION["role"] = "guest";
    $_SESSION["name"] = $_POST["name"];
    header("Location: list.php");
}

function user_is_valid($username, $password)
{
    $auth_file = __DIR__ . "/auth/" . $username . ".json";
    if (!file_exists($auth_file)) {
        return false;
    } else {
        $file = file_get_contents($auth_file);
        $json = json_decode($file, true);
        return $username == $json["username"] && $password == $json["password"];
    }
}

?>
<html>
<head>

</head>
<body>
<?php if (isset($_SESSION["name"])): ?>
    <?= htmlspecialchars($_SESSION["name"]) ?>
<?php else : ?>
    <form action="index.php" method="post">
        <table>
            <tr>
                <td>
                    <label for="name">Name: </label>
                </td>
                <td>
                    <input id="name" name="name" type="text" value=""/>
                </td>
            </tr>
            <tr>
                <td>

                    <label for="password">Password: </label>
                </td>
                <td>
                    <input id="password" name="password" type="password" value=""/>
                </td>
            </tr>
            <tr>
                <td></td>
                <td>
                    <input type="submit" value="login">
                </td>
            </tr>
        </table>
    </form>
<?php endif; ?>
</body>
</html>
