<?php

require_once 'main.conf.php';

session_start();
$isAuthorised = isset($_SESSION['isAuthorised']) ? $_SESSION['isAuthorised'] : null;

if ($isAuthorised == null) {
    header("Location: login.php");
    exit();
}
$userRepository = new UserRepository();

$currentUserId = isset($_SESSION['currentUserId']) ? $_SESSION['currentUserId'] : null;
$userIdToReAssign = isset($_POST['assigned_user_id']) ? $_POST['assigned_user_id'] : null;
$action = isset($_GET['action']) ? $_GET['action'] : null;
$taskId = isset($_GET['id']) ? $_GET['id'] : null;
$taskToAdd = isset($_POST['description']) ? $_POST['description'] : null;
$task = null;
$editValue = '';
$currentUser = $userRepository->getById($currentUserId);
$allUsers = $userRepository->getAll();
$taskRepository = new TaskRepository();

if ($userIdToReAssign) {
    $taskId = $_POST['task_id'];
    $userToReAssign = $userRepository->getById($userIdToReAssign);
}

if ($taskId) {
    $task = $taskRepository->getById($taskId);
}


switch ($action) {
    case 'delete':
        $taskRepository->deleteTask($task);
        break;
    case 'done':
        $task->setIsDone(true);
        $taskRepository->updateTask($task);
        break;
    case 'edit':
        $editValue = $task->getDescription();
        break;
}

if ($taskToAdd && $action == 'edit') {
    $task->setDescription($taskToAdd);
    $taskRepository->updateTask($task);
    header("Location: index.php");
    exit();
} elseif ($taskToAdd) {
    $task = new Task(null, $currentUser->getId(), $currentUser->getId(), $taskToAdd, false, null);
    $taskRepository->save($task);
} elseif ($userIdToReAssign) {
    $task->setAssignedUserId($userIdToReAssign);
    $taskRepository->updateTask($task);
}


if (isset($_POST['sort_by'])) {
    $order = $_POST['sort_by'];
    $tasks = $taskRepository->getAllOwnedByUser($order, $currentUserId);
    $myTasks = $taskRepository->getAllAssignedToMe($order, $currentUserId);
} else {
    $order = 'date_added';
    $tasks = $taskRepository->getAllOwnedByUser($order, $currentUserId);
    $myTasks = $taskRepository->getAllAssignedToMe($order, $currentUserId);
}

$template = $twig->loadTemplate("index.twig", null);
echo $template->render(array('tasks' => $tasks, 'allUsers' => $allUsers, 'myTasks' => $myTasks));
?>





