<?php

interface Repository
{

    public function save($object);

    public function getById($id);

}