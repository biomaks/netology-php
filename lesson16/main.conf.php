<?php

error_reporting(E_ALL);
require_once './vendor/autoload.php';

$loader = new Twig_Loader_Filesystem('./templates');
$twig = new Twig_Environment($loader, array(
    'cache' => './tmp/cache',
    'auto_reload' => true
));

$twig->addExtension(new Twig_Extension_Debug());
spl_autoload_register(function ($className) {
    $filePath = $_SERVER['DOCUMENT_ROOT'] . '/lesson16/classes/' . $className . '.php';
    if (file_exists($filePath)) {
        include $filePath;
    }
});


