<?php
require_once 'main.conf.php';
session_start();
$template = $twig->loadTemplate("login.twig", null);

$login = isset($_POST['login']) ? $_POST['login'] : '';
$password = isset($_POST['password']) ? $_POST['password'] : '';
$action = isset($_POST['action']) ? $_POST['action'] : null;
$error = false;
switch ($action) {
    case 'register' :
        register($login, $password);
        break;
    case 'login' :
        login($login, $password);
        break;
}

function login($login, $password)
{
    $userRepo = new UserRepository();
    $user = $userRepo->getUserByLogin($login);
    if ($user && $password == $user->getPassword()) {
        $_SESSION['isAuthorised'] = true;
        $_SESSION['username'] = $login;
        $_SESSION['currentUserId'] = $user->getId();
        header("Location: index.php");
        exit();
    } else {
        global $error;
        $error = true;
    }
}

function register($login, $password)
{
    $userRepo = new UserRepository();
    $user = new User(null, $login, $password);
    $userRepo->save($user);
    $user = $userRepo->getUserByLogin($login);
    $_SESSION['isAuthorised'] = true;
    $_SESSION['username'] = $login;
    $_SESSION['currentUserId'] = $user->getId();
    header("Location: index.php");
    exit();
}

echo $template->render(array('error' => $error));

?>



