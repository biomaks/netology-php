<?php

$file = file_get_contents("data.json");
$array = json_decode($file, true);

function handlePhones($phoneNumbersArray)
{
    $phoneNumberString = "";
    foreach ($phoneNumbersArray as $phoneNumber) {
        $phoneNumberString = $phoneNumberString . ", " . $phoneNumber;
    }
    return substr($phoneNumberString, 1);
}


?>
<html>
<head>
    <style>
        table, th, td {
            border: 1px solid black;
        }
    </style>

</head>
<body>
<table>
    <tr>
        <th> First Name</th>
        <th> Last Name</th>
        <th> City</th>
        <th> Postal Code</th>
        <th> Address</th>
        <th> Phones</th>
    </tr>

    <?php
    foreach ($array as $key => $value) {
        $firstName = $value['firstName'];
        $lastName = $value['lastName'];
        $addressArray = $value['address'];
        $city = $addressArray['city'];
        $postalCode = $addressArray['postalCode'];
        $streetAddress = $addressArray['streetAddress'];
        $phoneNumbers = handlePhones($value['phoneNumbers']);
        echo "<tr>
              <td>$firstName</td>
              <td>$lastName</td>
              <td>$city</td>
              <td>$postalCode</td>
              <td>$streetAddress</td>
              <td>$phoneNumbers</td>
              </tr>";
    }
    ?>
</table>
</body>
</html>