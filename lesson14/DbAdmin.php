<?php

class DbAdmin
{
    private $options = array(
        PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
    );

    protected $pdo;

    public function __construct()
    {
        $this->pdo = new PDO("mysql:host=127.0.0.1;dbname=global", "khalimov", "neto0641", $this->options);
    }

    public function getTables()
    {
        return $this->pdo->query("SHOW TABLES");
    }

    public function getTableInfo($tableName)
    {
        return $this->pdo->query("DESCRIBE $tableName");
    }

    public function getfieldInfo($tableName, $fieldName)
    {
        $tableInfo = $this->getTableInfo($tableName);
        foreach ($tableInfo as $field) {
            if ($field["Field"] == $fieldName) {
                return $field;
            }
        }
        return false;
    }

    public function renameColumn($table, $columnName, $newColumnName, $type)
    {
        $query = "ALTER TABLE $table CHANGE $columnName $newColumnName $type";
        $this->pdo->query($query);
    }

    public function alterColumn($table, $column, $type, $length, $is_null)
    {

        if ($length == 0 || $length == '') {
            $length = null;
        }
        $query = "ALTER TABLE $table MODIFY $column";

        if ($length) {
            $query = $query . " $type($length)";
        } else {
            $query = $query . " $type";
        }

        if (!$is_null) {
            $query = $query . " NOT NULL";
        }

        $this->pdo->query($query);
    }


}