<?php
require_once 'main.conf.php';

$tableName = isset($_GET['table']) ? $_GET['table'] : '';
$fieldName = isset($_GET['field']) ? $_GET['field'] : '';

$table = isset($_POST['table']) ? $_POST['table'] : '';
$field = isset($_POST['field']) ? $_POST['field'] : '';
$type = isset($_POST['type']) ? $_POST['type'] : '';
$length = isset($_POST['length']) ? intval($_POST['length']) : '';
$isNull = isset($_POST['is_null']) ? $_POST['is_null'] : '';
$action = isset($_POST['action']) ? $_POST['action'] : '';

$dbAdmin = new DbAdmin();


if ($action == 'edit') {
    $dbAdmin->alterColumn($table, $field, $type, $length, $isNull);
}

$fieldInfo = $dbAdmin->getfieldInfo($tableName, $fieldName);
?>

<html>
<head>
    <style>
        table {
            border-spacing: 0;
            border-collapse: collapse;
        }

        table td, table th {
            border: 1px solid #ccc;
            padding: 5px;
        }

        table th {
            background: #eee;
        }

        form {
            margin: 0;
        }
    </style>

</head>

<body>
<h1>Alter field: <?= $fieldName ?></h1>

<a href="/lesson14/index.php?table=<?= $tableName ?>"> << get back</a>

<h3>Table name : <?= $tableName ?></h3>
<div style="margin: 20px;">
    <form method="post">

        <input type="hidden" name="table" value="<?= $tableName ?>"/>
        <input type="hidden" name="field" value="<?= $fieldName ?>"/>
        <input type="hidden" name="action" value="edit"/>
        <table>
            <tbody>
            <tr>
                <th>Option</th>
                <th>Current value</th>
                <th>New value</th>
            </tr>
            <tr>
                <td>Column name</td>
                <td><?= $fieldName ?></td>
                <td><a href="/lesson14/renamecolumn.php?column=<?= $fieldName ?>&table=<?= $tableName ?>">rename</a>
                </td>
            </tr>
            <tr>
                <td>Type(Length)</td>
                <td><?= $fieldInfo['Type'] ?></td>
                <td>
                    <label for="datatype">datatype</label>
                    <select name="type" id="datatype">
                        <option value="varchar">varchar</option>
                        <option value="text">text</option>
                        <option value="int">int</option>
                        <option value="datetime">datetime</option>
                        <option value="tinyint">tinyint</option>
                        <option value="smallint">smallint</option>
                        <option value="smallint">mediumint</option>
                        <option value="bigint">bigint</option>
                        <option value="decimal">decimal</option>
                        <option value="float">float</option>
                        <option value="double">double</option>
                        <option value="date">date</option>
                        <option value="timestamp">timestamp</option>
                        <option value="time">time</option>
                        <option value="year">year</option>
                        <option value="char">char</option>
                        <option value="tinytext">tinytext</option>
                        <option value="mediumtext">mediumtext</option>
                        <option value="longtext">longtext</option>
                        <option value="enum">enum</option>
                        <option value="set">set</option>
                        <option value="bit">bit</option>
                        <option value="binary">binary</option>
                        <option value="varbinary">varbinary</option>
                        <option value="tinyblob">tinyblob</option>
                        <option value="blob">blob</option>
                        <option value="mediumblob">mediumblob</option>
                        <option value="longblob">longblob</option>
                        <option value="geometry">geometry</option>
                        <option value="point">point</option>
                        <option value="linestring">linestring</option>
                        <option value="polygon">polygon</option>
                        <option value="multipoint">multipoint</option>
                        <option value="multilinestring">multilinestring</option>
                        <option value="multipolygon">multipolygon</option>
                        <option value="geometrycollection">geometrycollection</option>
                    </select>
                    <br>
                    <label for="length">length</label>

                    <input id="length" type="text" name="length"/>
                </td>
            </tr>
            <tr>
                <td>IS NULL</td>
                <td><?= $fieldInfo['Null'] ?></td>
                <td>
                    <label for="is_null">

                    </label>
                    <select name="is_null" id="is_null">
                        <option value="false">NO</option>
                        <option value="true">YES</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td><input type="submit" value="save"/></td>
            </tr>
            </tbody>
        </table>
    </form>

</div>


</body>
</html>
