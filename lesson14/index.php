<?php
require_once 'main.conf.php';

$dbAdmin = new DbAdmin();

$tableName = isset($_GET['table']) ? $_GET['table'] : '';


$tables = $dbAdmin->getTables();
$showDescription = false;

if ($tableName) {
    $tableInfo = $dbAdmin->getTableInfo($tableName);
    $showDescription = true;
}
?>

<html>
<head>
    <style>
        table {
            border-spacing: 0;
            border-collapse: collapse;
        }

        table td, table th {
            border: 1px solid #ccc;
            padding: 5px;
        }

        table th {
            background: #eee;
        }

        form {
            margin: 0;
        }
    </style>

</head>
<body><h1>Анализируем таблицы в базе данных:</h1>
<div style="margin-bottom: 20px;">
    <form method="GET">
        <label for="select">Выберите таблицу:</label>
        <select id="select" name="table">
            <?php while ($row = $tables->fetch()): ?>
                <option value='<?= $row[0] ?>'><?= $row[0] ?></option>
            <?php endwhile; ?>
        </select>
        <input type="submit" value="Показать таблицу">
    </form>
</div>


<?php if ($showDescription): ?>
    <table>
        <tbody>
        <tr>
            <th>Field</th>
            <th>Type</th>
            <th>Action</th>
        </tr>
        <?php while ($row = $tableInfo->fetch()): ?>
            <tr>
                <td><?= $row["Field"] ?></td>
                <td><?= $row["Type"] ?></td>
                <td><a href="/lesson14/alter.php?table=<?= $tableName ?>&field=<?= $row["Field"] ?>">alter</a></td>
            </tr>
        <?php endwhile; ?>
        </tbody>
    </table>
<?php endif; ?>

</body>
</html>