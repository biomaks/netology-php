<?php

require_once 'main.conf.php';

$table = isset($_GET['table']) ? $_GET['table'] : '';
$column = isset($_GET['column']) ? $_GET['column'] : '';

$newColumnName = isset($_POST['new_column_name']) ? $_POST['new_column_name'] : '';
$dbAdmin = new DbAdmin();

if ($newColumnName) {
    $fieldInfo = $dbAdmin->getfieldInfo($table, $column);
    $dbAdmin->renameColumn($table, $column, $newColumnName, $fieldInfo['Type']);
    header("Location: alter.php?table=$table&field=$newColumnName");
    exit();
}

?>

<html>
<head>
    <style>
        table {
            border-spacing: 0;
            border-collapse: collapse;
        }

        table td, table th {
            border: 1px solid #ccc;
            padding: 5px;
        }

        table th {
            background: #eee;
        }

        form {
            margin: 0;
        }
    </style>

</head>
<body>
<h1>Renaming column:</h1>

<form method="post">
    <label> New column name
        <input type="text" name="new_column_name" value="<?= $column ?>"/>
    </label>
    <input type="submit" value="save"/>

</form>

</body>
</html>
